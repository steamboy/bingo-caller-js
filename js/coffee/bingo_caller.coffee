BingoCaller = BingoCaller or {}

BingoCaller =
  initialize: ->
    # Set min/max
    @min = 1
    @max = 90

    @renderBoard()
    @actions()

  # Render Bingo Board
  renderBoard: ->
    count = @max
    box   = ''
    while count >= @min
      box = "<div class='#{count} number-container'><div class='number'>#{count}</div></div>" + box
      count--

    $('#board').append(box)

  actions: ->
    $('#call_number').click (e) ->
      BingoCaller.numberHighlighter()

    $(document).on 'click', '.current-number', ->
      $('.big-number').html($(this).text()).toggle()

  numberHighlighter: ->
    # Check if all numbers are highlighted
    if $('#board .highlight').size() is 90
      $('#alert').modal()
      return false

    number = @randomIntFromInterval()
    console.log number
    # Check if number is already highlighted
    number_container = $('#board').find(".#{number}")
    if number_container.hasClass('highlight')
      @numberHighlighter()
    else
      number_container.addClass('highlight')
      # Show recent called number
      @showCalledNumbers(number)

  showCalledNumbers: (number) ->
    parent  = ".called-numbers-container"
    child   = ".called-numbers"
    classes = "btn-lg current-number"
    $("#{parent}").append("<button type='button' class='called-numbers btn btn-warning'>#{number}</button>")
    $("#{parent} #{child}:first").remove() if $("#{parent} #{child}").size() > 2
    $("#{parent} #{child}").removeClass("#{classes}")
    $("#{parent} #{child}:last").addClass("#{classes}")
    $('.big-number').html(number)


  randomIntFromInterval: ->
    Math.floor Math.random() * (@max - @min + 1) + @min

$ ->
  BingoCaller.initialize()